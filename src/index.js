import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, HashRouter, Route, Switch } from "react-router-dom";
import blogdata from './data/blogdata.json';
import Singleblogdata from './data/single-blogdata.json';
import HomeV1 from './components/home-v1';
import HomeV2 from './components/home-v2';
import About from './components/about';
import Services from './components/services';
import Blog from './components/blog';
import BlogGrid from './components/blog-grid';
import BlogDetails from './components/blog-details';
import Contact from './components/contact';

class Root extends Component {
    render() {
        return(
            <Router>
                <HashRouter basename="/">
                <div>
                <Switch>
                    <Route exact path="/" component={HomeV1} />
                    <Route path="/home-v2" component={HomeV2} />
                    <Route path="/about" component={About} />
                    <Route path="/services" component={Services} />
                    <Route path="/blog" render={ () => { return <Blog data={blogdata} /> }} />
                    <Route path="/blog-grid" render={ () => { return <BlogGrid data={blogdata} /> }} />
                    <Route path="/blog-details" render={ () => { return <BlogDetails data={Singleblogdata} /> } } />
                    <Route path="/contact" component={Contact} />
                </Switch>
                </div>
                </HashRouter>
            </Router>
        )
    }
}

export default Root;

ReactDOM.render(<Root />, document.getElementById('plumbio'));
