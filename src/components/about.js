import React from 'react';
import Header from './global-components/header';
import PageHeader from './global-components/page-header';
import VideoV2 from './section-components/video-v2';
import WhatWeDo from './section-components/what-we-do';
import TestimonialV2 from './section-components/testimonial-v2';
import Team from './section-components/team';
import Subscribe from './section-components/subscribe';
import FooterV1 from './global-components/footer-v1';

const About = () => {
    return <div>
        <Header />
        <PageHeader headertitle="About" />
        <VideoV2 customclass="pd-top-120 bg-none" />
        <WhatWeDo />
        <Team />
        <TestimonialV2 />
        <Subscribe />
        <FooterV1 />
    </div>
}

export default About

