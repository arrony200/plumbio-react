import React, { Component } from 'react';
import sectiondata from '../../data/sections.json';
import parse from 'html-react-parser';

class Features extends Component {
    render() {
        let publicUrl = process.env.PUBLIC_URL+'/'
        let imgattr = 'image'

    return <div>
            <div className="sba-featute-area">
                <div className="container">
                    <div className="row custom-gutters-16 justify-content-center">
                        <div className="col-xl-4 col-lg-9">
                            <div className="section-title style-two text-xl-left text-center">
                                <h2 className="title">{parse(sectiondata.features.sectiontitle)} <span>{sectiondata.features.sectiontitle_color}</span></h2>
                                <p>{sectiondata.features.short_description}</p>
                                <a className="read-more" href={sectiondata.features.btn_url}>{sectiondata.features.btn_text} <i className="la la-long-arrow-right"></i></a>
                            </div>
                        </div>
                        
                        <div className="col-xl-4 col-sm-6">
                            <div className="single-feature-left">
                            {sectiondata.features.featurescontent1.map( ( feature, i ) => 
                                <div key={i} className="single-feature">
                                    <div className="media">
                                        <img className="media-left" src={publicUrl+feature.icon} alt={imgattr}/>
                                        <div className="media-body">
                                            <h6>{feature.title}</h6>
                                            <p>{feature.content}</p>
                                        </div>
                                    </div>
                                </div>
                            )}
                            </div>
                        </div>
                        <div className="col-xl-4 col-sm-6 mg-top-80">
                            <div className="single-feature-right">
                            {sectiondata.features.featurescontent1.map( ( feature, i ) => 
                                <div key={i} className="single-feature">
                                    <div className="media border-radius-3">
                                        <img className="media-left" src={publicUrl+feature.icon} alt={imgattr}/>
                                        <div className="media-body">
                                            <h6>{feature.title}</h6>
                                            <p>{feature.content}</p>
                                        </div>
                                    </div>
                                </div>
                            )}
                               
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    }
}

export default Features

