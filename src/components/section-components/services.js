import React, { Component } from 'react';
import sectiondata from '../../data/sections.json';

class Services_V1 extends Component {

    render() {
        let publicUrl = process.env.PUBLIC_URL+'/'
        let anchor = '#'
        let imgattr = 'image'
        let customclass = this.props.customclass ? this.props.customclass : ''
   
        return <div>
                <div className={"service-area " +customclass}>
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-xl-7 col-lg-10">
                                <div className="section-title text-center margin-bottom-90">
                                <h2 className="title wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.1s">{sectiondata.services.sectiontitle} <span>{sectiondata.services.sectiontitle_color}</span>{sectiondata.services.sectiontitle_2}</h2>
                                <p className="wow animated fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.2s">{sectiondata.services.short_description}</p>
                                </div>
                            </div>
                        </div>
                        <div className="row custom-gutters-16">
                        {sectiondata.services.items.map( ( item, i ) => 
                            <div key={i} className="col-xl-3 col-md-6 col-sm-6">
                                <div className="single-service wow animated fadeInUp" data-wow-duration="0.4s" data-wow-delay="0.1s">
                                    <img src={publicUrl+item.icon} alt={imgattr} />
                                    <h6><a href={anchor}>{item.title}</a></h6>
                                    <p>{item.content}</p>
                                    <div className="read-more">
                                        <a href={item.url}><img src={publicUrl+'assets/img/service/arrow.png'} alt={imgattr}/></a>
                                    </div>
                                </div>
                            </div>
                        )}
                        </div>
                    </div>
                </div>

    <div className="section-indent">
		<div className="container container__fluid-xl">
			<div className="row">
				<div className="col-md-5 col-lg-6">
					<div className="blocktitle text-left blocktitle__nomargin">
						<div className="blocktitle__under">Services</div>
						<div className="blocktitle__subtitle">our services</div>
						<div className="blocktitle__title">From Leaking Faucet <br/> to Gushing Pipes</div>
					</div>
				</div>
				<div className="col-md-6 col-lg-6 tt-hide__mobile">
					<div className="tt-notes">
						While certain plumbing issues, such as a minor toilet clog, can be quickly addressed with do-it-yourself methods, most plumbing problems require the assistance of a professional.
					</div>
				</div>
			</div>
		</div>
	</div>
	<div className="section-indent02">
		<div className="container-fluid container__fluid-xl">
			<div className="swiper-container tt-swiper__noshadow col-inner-lg"
				data-carousel="swiper"
				data-space-between="30"
				data-slides-xl="3"
				data-slides-lg="3"
				data-slides-md="2"
				data-slides-sm="1">
				<div className="swiper-wrapper">
                {sectiondata.services.items.map( ( item, i ) => 
					<div className="swiper-slide">
						<a href="#" className="imgbox-inner">
							<div className="imgbox-inner__img">
								<picture>
                                <img src={publicUrl+item.icon} alt={imgattr} />
								</picture>
							</div>
							<div className="imgbox-inner__description-small">
								<div className="imgbox-inner__title">
									<div className="tt-icon icon-694055"><i className="icon-2373426"></i></div>
									<div className="tt-title">
										<div className="tt-text-01">Residential</div>
										<div className="tt-text-02">Services</div>
									</div>
								</div>
								<div className="tt-icon-box">+</div>
							</div>
							<div className="imgbox-inner__description-large">
								<div className="tt-align">
									<div className="imgbox-inner__title">
										<div className="tt-icon icon-694055"><i className="icon-2373426"></i></div>
										<div className="tt-title">
											<div className="tt-text-01">Residential</div>
											<div className="tt-text-02">Services</div>
										</div>
									</div>
									<p>
										Keep it as comfortable as possible by making sure your plumbing system is working at its best. Here’s just a sampling of the services we provide for our customers.
									</p>
									<ul className="tt-list tt-offset__01 tt-list__color02">
										<li>Plumbing Repairs &amp; Installation</li>
										<li>Drain &amp; Sewer Cleaning/Replacement</li>
										<li>Water Heaters</li>
										<li>Water Line Installation</li>
										<li>Sump &amp; Sewage Pumps</li>
									</ul>
								</div>
								<div className="tt-external-link icon-545682"></div>
							</div>
						</a>
					</div>
                    )}
				</div>
				<div className="swiper-pagination swiper-pagination__align01 swiper-pagination__center"></div>
			</div>
		</div>
	</div>

            </div>
     }
}

export default Services_V1