import React, { Component } from 'react';
import sectiondata from '../../data/sections.json';
import parse from 'html-react-parser';

class Slider_V1 extends Component {

    render() {

        let imgalt = 'image'
        let publicUrl = process.env.PUBLIC_URL+'/'
        let data = sectiondata.slider_v1

    return <div>
    <div className="section-indent nomargin">
		<div className="mainslider__wrapper">
			<div className="swiper-container mainslider" id="js-mainslider" data-arrow="visible-desktop">
				<div className="swiper-wrapper">
                {data.items.map( ( item, i ) =>
					<div className="swiper-slide">
						<div className="mainslider__imgbg">
							<div className="slide-inner">
									<img src={publicUrl+item.bg_image_url} alt={imgalt}/>
							</div>
						</div>
						<div className="mainslider__img">
							<picture>
								<img src={publicUrl+item.image_url1} alt={imgalt}/>
							</picture>
						</div>
						<div className="mainslider__holder layout-h-r mainslider__layout01">
							<div className="mainslider__limiter">
								<div className="mainslider__title">
									<a href="https://vimeo.com/20023435" className="glightbox3 tt-btn__video icon-808557"><span></span></a>
									<div className="mainslider__title-text">
                                    {item.title1}
									</div>
								</div>
								<div className="mainslider__text">
                                {parse( item.description )}
								</div>
							</div>
						</div>
					</div>
                    )}
				
				</div>
				<div className="mainslider__button mainslider__button-next"><i className="icon-545682"></i></div>
				<div className="mainslider__button mainslider__button-prev"><i className="icon-545682"></i></div>
			</div>
		</div>
	</div>

            </div>
    }
}

export default Slider_V1

