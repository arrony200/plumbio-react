import React, { Component } from 'react';
import footerdata from '../../data/footerdata.json';

class Footer_v1 extends Component {

    componentDidMount() {
        let publicUrl = process.env.PUBLIC_URL+'/'

        const minscript = document.createElement("script");
        minscript.async = true;
      //  minscript.src = publicUrl + "assets/js/bundle.js";

        document.body.appendChild(minscript);
    }

    render() {

        let publicUrl = process.env.PUBLIC_URL+'/'
        let imgattr = "Footer logo"
        const inlineStyle = {
            backgroundImage: 'url('+publicUrl+footerdata.footershape+')'
        }

        return (
            <div>
            
                <footer id="tt-footer">
                    <div id="map" className="lazyload" data-bg="images/map-footer.jpg">
                    <iframe src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" className="lazyload" data-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3111.5820008392016!2d-77.45701318432829!3d38.75035326320268!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89b65b93b943fb07%3A0xc8712e829da92b9e!2sHistoric%20District%2C%208494%20Signal%20Hill%20Rd%2C%20Manassas%2C%20VA%2020110%2C%20USA!5e0!3m2!1sen!2sua!4v1605172763191!5m2!1sen!2sua" allowfullscreen="" aria-hidden="false"></iframe>
                </div>
                <div className="footer-layout lazyload" data-bg="images/map-footer.png">
                    <div className="container container__fluid-lg">
                        <div className="row f-info__wrapper">
                            <div className="col-sm-4">
                                <div className="f-info">
                                    <div className="f-info__icon icon-694055"><i className="icon-786204"></i></div>
                                    <div className="f-info__title">Head Office:</div>
                                    8494 Signal Hill Road Manassas, VA, 20110
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="f-info">
                                    <div className="f-info__icon icon-694055"><i className="icon-126509"></i></div>
                                    <div className="f-info__title">Phone Numbers:</div>
                                    <address>123 456 789</address>
                                    Emergency
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="f-info">
                                    <div className="f-info__icon icon-694055"><i className="icon-9927001"></i></div>
                                    <div className="f-info__title">Working Time:</div>
                                    24 Hour Service - 7 Days a Week 365 Days
                                </div>
                            </div>
                        </div>
                        <div className="f-logo-layout text-center">
                         
                            <a href="/"><img className="f-logo lazyload" src={publicUrl+footerdata.footerlogo} alt={imgattr} /></a>
                           
                            <p className="f-min-width">
                                Our plumbing contractors provide courteous, friendly, affordable, and effective residential plumbing services.
                            </p>
                            <div className="text-center">
                                <ul className="f-social-icon">
                                    <li><a href="https://twitter.com/" className="icon-733635"></a></li>
                                    <li><a href="https://www.facebook.com/" className="icon-59439"></a></li>
                                    <li><a href="https://www.linkedin.com/" className="icon-2111532"></a></li>
                                    <li><a href="https://skype.com/" className="icon-733626"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="footer-copyright">
                    &copy; 2021 Plumbio. All Rights Reserved.
                </div>
                </footer>

            </div>
        )
    }
}


export default Footer_v1