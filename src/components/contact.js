import React from 'react';
import Header from './global-components/header';
import PageHeader from './global-components/page-header';
import ContactForm from './section-components/contact-form';
import ContactInfo from './section-components/contact-info';
import FooterV2 from './global-components/footer-v2';

const Contact = () => {
    return <div>
        <Header />
        <PageHeader headertitle="Contact" />
        <ContactForm />
        <ContactInfo />
        <FooterV2 />
    </div>
}

export default Contact

